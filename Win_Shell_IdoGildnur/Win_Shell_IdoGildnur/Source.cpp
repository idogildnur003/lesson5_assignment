#include "Helper.h"
#include <string>
#include <Windows.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define BUFFSIZE MAX_PATH

static string PATH;

typedef int(__stdcall *f_funci)();


/*
The function returns the path of the running directory.
input:
	None.
output:
	The current selected path.
*/
string pwd()
{
	TCHAR Buffer[BUFFSIZE];//the input buffer of the path
	DWORD dwRet;
	dwRet = GetCurrentDirectory(BUFFSIZE, Buffer);

	if (dwRet == 0)
	{
		printf("GetCurrentDirectory failed (%d)\n", GetLastError());
		return "";
	}
	else
		return Buffer;
}


/*
The function resets the current selected directory path according to the user's input.
input:
	Selected directory path.
output:
	None.
*/
void enterDir(string p)
{
	if (!SetCurrentDirectory(p.c_str()))
		printf("SetCurrentDirectory failed (%d)\n", GetLastError());
}


/*
The function creates a new file in the current running directory.
input:
	Current directory path.
output:
	None.
*/
void createFile(string p)
{
	BOOL bTest = FALSE;
	DWORD dwNumRead = 0;
	HANDLE hFile = CreateFile(p.c_str(), GENERIC_READ, FILE_SHARE_READ,
		NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	bTest = CloseHandle(hFile);
	if (hFile == 0)
		printf("CreateFile failed (%d)\n", GetLastError());
}


/*
The fuction displays all the files in the running directory.
input:
	None.
output:
	All displayed files in the current running directory.
*/
string ls()
{
	string str = "";
	std::string pattern(pwd());
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			str = str + "\n" + data.cFileName;
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
	return str;
}


int main()
{
	string n = "";
	while (true)
	{
		cout << ">>>";
		std::getline(cin, n);
		string cmd = Helper::get_words(n)[0];

		if (cmd == "create")
		{
			createFile(Helper::get_words(n)[1]);
		}

		else if (cmd == "pwd")
		{
			cout << pwd() << endl;
		}

		else if (cmd == "cd")
		{
			enterDir(Helper::get_words(n)[1]);
		}

		else if (cmd == "ls")
		{
			cout << ls() << endl;
		}
	
	}
	return 1;
}

